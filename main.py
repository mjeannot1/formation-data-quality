import prefect
from prefect import task, Flow


def main():
    flow = build_pipeline()

    flow.visualize()
    # flow.run()


def build_pipeline() -> Flow:
    with Flow("main-pipeline") as flow:
        say_hello()

    return flow


@task()
def say_hello():
    logger = prefect.context.get("logger")
    logger.info("Hello world!")


if __name__ == "__main__":
    main()
