import great_expectations as ge

context = ge.data_context.DataContext()

# Construction des Expectations avec un jeu de données 100% correct
train = ge.read_csv("data/train.csv")

# la colonne ID doit exister
train.expect_column_to_exist("id")

# compléter




# Verification que le jeu de données est bien correct
validation_results = train.validate(expectation_suite="my_expectations.json")
if validation_results["success"]:
    print("Train OK")
else:
    print(validation_results)
    print("!Train KO!")

# Sauvegarde des expérimentations même si une des règles est en "failed"
train.save_expectation_suite("my_expectations.json", discard_failed_expectations=False)


# Dataset "live"
test = ge.read_csv("data/live.csv")

# compléter pour vérifier si le jeu de données est correct